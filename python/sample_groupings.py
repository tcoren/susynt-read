#!/bin/env python

import os
import sys
import argparse
import subprocess
import glob
import json

def get_process_data(args) :

    data = {}

    # grab the user-specified one
    if args.process_file != '' :
        filename = args.process_file
        if not os.path.isfile(filename) :
            print 'ERROR Cannot find user-requested process file (=%s)' % filename
            sys.exit()
        with open(filename, 'r') as json_data :
            data = json.load(json_data)

    # get the default one
    else :
        pwd = os.getcwd()
        req_dir = 'susynt-read'
        if req_dir not in pwd :
            print 'ERROR Cannot find \"%s\" directory in current working directory (=%s)' % (req_dir,pwd)
            sys.exit()

        path = pwd[:pwd.find(req_dir) + len(req_dir)]

        data_dir = path + '/data/'
        if not os.path.isdir(data_dir) :
            print 'ERROR Cannot find data/ directory in susynt-read directory (=%s)' % path
            sys.exit()

        default_name = 'process_groups.json'
        filename = data_dir + default_name

        if not os.path.isfile(filename) :
            print 'ERROR Cannot find default process file (=%s)' % filename
            sys.exit()

        with open(filename, 'r') as json_data :
            data = json.load(json_data)

    return data

def get_dsid_from_sample(text_file) :

    look_for = 'mc16_13TeV.'
    dsid_loc = text_file.find(look_for)
    dsid = text_file[dsid_loc + len(look_for):]
    dsid = dsid.split('.')[0]
    return dsid

def make_dir_for_process(args, all_text_files, process_name, dataset_list) :

    input_dir = os.path.abspath(args.input)
    output_dir = '/'.join(input_dir.split('/')[:-1])
    dsids_for_process = [ int(s.strip().split('.')[0]) for s in dataset_list ]

    # make the output dir
    dirname = process_name
    if 'mc16a' in input_dir.split('/')[-1] :
        dirname += '_mc16a'
    elif 'mc16d' in input_dir.split('/')[-1] :
        dirname += '_mc16d'
    elif 'mc16e' in input_dir.split('/')[-1] :
        dirname += '_mc16e'
    output_dir += '/%s/' % dirname
    cmd = 'mkdir -p %s' % output_dir
    subprocess.call(cmd, shell = True)

    # copy the lists
    lists_we_want = []
    n_expected = len(dataset_list)
    dsids_we_got = []
    for txt in all_text_files :
        txt_dsid = int(get_dsid_from_sample(txt))
        if txt_dsid in dsids_for_process :
            dsids_we_got.append(txt_dsid)
            lists_we_want.append(txt)

    if len(lists_we_want) != n_expected :
        print '\n'
        print 'WARNING Did not find expected number of lists for %s (expected %d, got %d)' % (process_name, n_expected, len(dsids_we_got))
        print '         > DSIDS Expected:'
        for d in dsids_for_process :
            print '             %d' % d
        print '         > DSIDS Found:'
        for d in dsids_we_got :
            print '             %d' % d
        print '\n'

    if args.verbose: print "%s) Addings %d file lists" % (process_name, len(lists_we_want))
    for filelist in lists_we_want :
        cmd = "cp %s %s" % (os.path.abspath(filelist), output_dir)
        subprocess.call(cmd, shell = True)

def main() :

    parser = argparse.ArgumentParser('Break up a sample list dir into process groups')
    parser.add_argument('-i', '--input', required = True,
        help = 'Provide the directory with all of the sample list dids (the super-set dir)')
    parser.add_argument('-p', '--process-file', default = '',
        help = 'Provide a specific process grouping (JSON) file')
    parser.add_argument('--select-process', default = '',
        help = 'Request a specific process to make grouping for')
    parser.add_argument('-v', '--verbose', default = False, action = 'store_true',
        help = 'Turn on verbosity')
    args = parser.parse_args()

    process_dict = get_process_data(args)
    process_names = process_dict.keys()

    if args.select_process != '' :
        if args.select_process not in process_names :
            print 'ERROR Requested process (=%s) does not appear in grouping file' % args.select_process
            sys.exit()

    if args.verbose :
        print 35 * '-'
        print 'Found %d processes:' % len(process_names)
        for i, pname in enumerate(process_names) :
            print ' [%02d] %s' % (i,pname)
        print 35 * '-'

    all_text_files = glob.glob('%s/*.txt' % os.path.abspath(args.input))
    if len(all_text_files) == 0 :
        print 'ERROR Found no text files in input directory (=%s)' % os.path.abspath(args.input)
        sys.exit()

    for process_name, dataset_list in process_dict.items() :
        if args.select_process != '' and process_name != args.select_process : continue
        make_dir_for_process(args, all_text_files, process_name, dataset_list)

#_____________________________
if __name__ == '__main__' :
    main()
