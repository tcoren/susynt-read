#!/bin/env/python

######################################################
# this script will make a list of the gobal datasets 
# for a given input text file that contains on each
# line a file container name
#
# Requirements:
#  - lsetup rucio
#  - voms-init-proxy -voms atlas -valid 96:00
#
# daniel.joseph.antrim@CERN.CH
# October 2018
######################################################

import os
import sys
import argparse
import subprocess

def is_rucio_setup() :
    return os.environ.get('RUCIO_AUTH_TYPE')

def get_containers(args) :

    '''
    Opens the input provided by the user, reads
    each line, and appends each line to an output
    list. Skips all lines starting with # (comments).

    Arguments:
        args : input command line arguments

    Returns:
        builds a generator of files
    '''

    container_list = []
    with open(args.input, 'r') as input_file :
        for line in input_file :
            line = line.strip()
            if not line : continue
            if line.startswith('#') : continue
            container_list.append(line)
    return container_list

def get_dtn_from_pfn(fax_name) :

    """
    From a given rucio LFN return the data endpoint
    identifier, e.g. from

    root://griddev03.slac.stanford.edu:2094//xrootd/atlas/atlaslocalgroupdisk/rucio/group/phys-susy/bc/b1/group.phys-susy.16828738._000533.susyNt.root

    return root://griddev03.slac.stanford.edu:2094//xrootd/atlas/atlaslocalgroupdisk/rucio/
    """
    
    look_for = 'rucio/'
    dtn = fax_name.strip()[: fax_name.strip().find(look_for) + len(look_for)]
    return dtn

def get_filename_from_pfn(fax_name) :

    """
    From a given rucio LFN return the actual filename
    which is the same across DTN, e.g. from

    root://griddev03.slac.stanford.edu:2094//xrootd/atlas/atlaslocalgroupdisk/rucio/group/phys-susy/bc/b1/group.phys-susy.16828738._000533.susyNt.root

    return /group/phys-susy/bc/b1/group.phys-susy.16828738._000533.susyNt.root
    """

    look_for = 'rucio/'
    filename = fax_name.strip()[fax_name.strip().find(look_for)+len(look_for):]
    return filename

def get_fax_names(container) :

    tmp_name = 'tmp_GLFN_%s.txt' % container[:-2]
    cmd = 'rucio list-file-replicas --protocol root --pfns %s > %s' % (container, tmp_name)
    subprocess.call(cmd, shell = True)
    fax_struct = {}
    with open(tmp_name, 'r') as glfn_file :
        for line in glfn_file :
            line = line.strip()
            if not line : continue
            dtn = get_dtn_from_pfn(line)
            filename = get_filename_from_pfn(line)
            if dtn not in fax_struct :
                fax_struct[dtn] = set()
                fax_struct[dtn].add(filename)
            else :
                fax_struct[dtn].add(filename)

    cmd = 'rm %s' % tmp_name
    subprocess.call(cmd, shell = True)
    return fax_struct
    
def get_bare_names(fax_names) :

    out = []
    for name in fax_names :
        bare = name.split("rucio/")[-1]
        bare = "/" + bare
        out.append(bare)
    out = list(set(out))
    return out

def get_output_name(container, verbose) :

    name = container
    if ':' in name :
        name = name.split(':')[1]
    if name.endswith('/') :
        name = name[:-1]
    if name.endswith('_nt') :
        name = name[:-3]
    name = name + '.txt'
    return name

def get_nice_dtn_name(dtn) :

    if dtn == '' :
        return 'base'

    out = dtn.replace('root://','')
    out = out.split(':')[0]
    which = ''
    if 'scratch' in dtn :
        which = 'scratch'
    elif 'groupdisk' in dtn :
        which = 'groupdisk'
    if which != '' :
        out += '_%s' % which
    return out

def mv_file_to_dir(output_filename, output_dir) :

    if not os.path.isdir(output_dir) :
        cmd = 'mkdir -p %s' % str(output_dir)
        subprocess.call(cmd, shell = True)

    cmd = 'mv %s %s' % (output_filename, output_dir)
    subprocess.call(cmd, shell = True)

def mv_file_to_dir_dtn(output_filename, output_dir, dtn = '') :

    if not os.path.isdir(output_dir) :
        cmd = 'mkdir -p %s' % str(output_dir)
        subprocess.call(cmd, shell = True)

    dtn_nice = get_nice_dtn_name(dtn)
    dtn_subdir = '%s/filelists_%s/' % (str(output_dir), dtn_nice)
    if not os.path.isdir(dtn_subdir) :
        cmd = 'mkdir -p %s' % str(dtn_subdir)
        subprocess.call(cmd, shell = True)

    cmd = 'mv %s %s' % (output_filename, dtn_subdir)
    subprocess.call(cmd, shell = True)

def build_dataset_list(args, containers) :

    n_total = len(containers)
    for icontainer, container in enumerate(containers) :
        if args.verbose :
            print '[%d/%d] %s' % (icontainer+1, n_total, container)
        fax_struct = get_fax_names(container)

        if not fax_struct :
            print 'WARNING Returned empty fax struct for container %s' % container
            continue

        dtn_with_max_files = ''
        max_f = -1

        all_files = set()
        group_disks = []

        for dtn, filenames in fax_struct.items() :
            #output_filename = get_output_name(container, dtn, fax_struct[dtn], args.verbose)
            output_filename = get_output_name(container, args.verbose)

            if 'groupdisk' in dtn :
                group_disks.append(dtn)

            if len(filenames) > max_f :
                dtn_with_max_files = dtn
                max_f = len(filenames)

            with open(output_filename, 'w') as output_file :
                for f in filenames :
                    all_files.add(f) 
                    add_it = True
                    if dtn.endswith('/') and not f.startswith('/') :
                        add_it = False
                    if not dtn.endswith('/') and f.startswith('/') :
                        add_it = False
                    if dtn.endswith('/') and f.startswith('/') :
                        f = f[1:]
                        add_it = False
                    name_with_dtn = dtn
                    if add_it :
                        name_with_dtn += '/'
                    name_with_dtn += f
                    output_file.write(name_with_dtn + '\n')
            mv_file_to_dir_dtn(output_filename, args.output, dtn = dtn)

        # make the base dir
        output_filename = get_output_name(container, args.verbose)
        filenames = fax_struct[dtn_with_max_files]
        with open(output_filename, 'w') as output_file :
            for f in filenames :
                all_files.add(f)
                output_file.write(f + '\n')
        mv_file_to_dir_dtn(output_filename, args.output, dtn = '')

        # ensure that files located on the groupdisks are complete
        for dtn in group_disks :
            files_for_group = fax_struct[dtn]
            delta = set(all_files) - set(files_for_group)
            if delta :
                print "WARNING GROUPDISK %s missing files (expect: %d, got: %d) for %s" % (get_nice_dtn_name(dtn), len(all_files), len(all_files) - len(delta), container.split(':')[1])
                n_missing = len(delta)
                for i, d in enumerate(delta) :
                    print "WARNING GROUPDISK      > [%02d/%02d] Missing: %s" % (i+1, n_missing, d)

def main() :

    parser = argparse.ArgumentParser(description = 'Create a list of global dataset identifiers')
    parser.add_argument('-i', '--input',  help = 'Provide a *.txt file containing the list of dataset containers', required = True)
    parser.add_argument('-o', '--output', help = 'Provide the output directory to store the outputs in', default = './')
    parser.add_argument('-v', '--verbose', help = 'Print out stuff', default = False, action = 'store_true')
    args = parser.parse_args()

    if not os.path.isfile(args.input) :
        print 'ERROR Provided input (=%s) is not found' % args.input
        sys.exit()

    if not is_rucio_setup() :
        print 'ERROR It looks like rucio is not setup, please do this before calling this script'
        sys.exit()

    input_containers = get_containers(args)
    n_containers = len(input_containers)
    if args.verbose :
        print 'Found %d input containers' % n_containers
    build_dataset_list(args, input_containers)

    

#___________________________
if __name__ == '__main__' :
    main()
