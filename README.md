susynt-read
===========

# Contents
* [Introduction](#introduction)
* [Requirements](#requirements)
  * [Setting up your athena cache](#setting-up-your-athena-cache)
* [Actions](#actions)
  * [First Time Setup](#first-time-setup)
  * [Subsequent Area Setup](#subsequent-area-setup)
  * [Subsequent Compilation](#subsequent-compilation)
  * [Compiling After Changes to CMakeLists](#compiling-after-changes-to-cmakelists)
* [Useful Scripts](#useful-scripts)
  * [Listing Available Datasets](#listing-available-datasets)
  * [Making Condor Filelists](#making-condor-filelists)

## Introduction
This packages prepares an area for reading susyNt files for physics analysis.

## Requirements
We assume that you have access to **cvmfs**, for setting up the **AnalysisBase** release, as well as **svn**:

1) access to **cvmfs** (be on a machine with cvmfs)

2) ATLAS environment is setup (run: ```source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh```)

3) kerberos tickets (run: ```kinit -f ${USER}@CERN.CH```)

### Setting up your *athena* cache

The software that is setup by the *susynt-read* scripts (specifically SusyNtuple) relies on [atlas/athena](https://gitlab.cern.ch/atlas/athena). 
This means that that in order for it to work we need to **sparse checkout** the [atlas/athena](https://gitlab.cern.ch/atlas/athena) repository. 
If you are unfamiliar with what this means please go directly to the [ATLAS Git Workflow Tutorial](https://atlassoftwaredocs.web.cern.ch/gittutorial/) before moving ahead.

Performing a fresh sparse-checkout can be a time consuming operation given the abusive
nature [atlas/athena's](https://gitlab.cern.ch/atlas/athena) use of *git*. Rather than do
this it is recommended that you have a "git cache" that you checkout in a common, safe
area and then bounce any subsequent sparse checkouts *off of that already cloned repository*.
This is described in the [LearningGit Twiki](https://twiki.cern.ch/twiki/bin/view/Main/LearningGit#2_Creating_your_cache_repository).

The installation of the software will go much faster if you have such a "git cache" installed. By default,
the SusyNtuple-related software will expect this cache to be located in
`${HOME}/susynt_gitcache/athena`. You can obtain such a cache by doing the following steps:

```bash
cd ${HOME}
mkdir susynt_gitcache
lsetup git
git atlas init-workdir https://:@gitlab.cern.ch:8443/susynt/athena.git
```

Using a git cache of the athena repository will *significantly* speed up any subsequent work dir initialization.


## Actions

### First Time Setup

Here are the steps to setup an area from scratch.

```
git clone -b <tag> https://:@gitlab.cern.ch:8443/susynt/susynt-read.git
cd susynt-read/
source bash/setup_area.sh
source bash/setup_release.sh --compile
```
**Note**: `git clone -b <tag>` may not work in older versions of git. In that case checkout the repository and then run `git checkout tags/<tag>`.

**Note**: by default the script assumes that you both wish to use a git cache (as described above)
and that that cache is located in the default location of `${HOME}/susynt_gitcache/athena`. If you
wish to use another gitcache, you can provide its location by adding to the above third line:

```bash
source bash/setup_area.sh --gitcache <path-to-cache>
```

If you wish to skip the use of a git cache altogether and do the full sparse checkout of 
[atlas/athena](https://gitlab.cern.ch/atlas/athena) instead, do:

```bash
source bash/setup_area.sh --no-cache
```

The script *bash/setup_area.sh* call in the above snippet will checkout the "stable" release given by the tag ```<tag>```. All of the tags are listed in the **releases** section [here](https://github.com/susynt/susynt-read/releases). This means that it will checkout the associated tags of **SusyNtuple** and **SUSYTools** (i.e. those tags of these two packages that were used to build susyNt tag ```<tag>```). You can use the ```-h``` or ```--help``` option to see the full list of options:

```bash
source bash/setup_area.sh --help
```

You will see that you can give the ```--sn``` option to specify a specific tag (or branch) of **SusyNtuple**. 

The script *bash/setup_release.sh* sets up the associated **AnalysisBase** release. When given the ```--compile``` flag it will also run the full compilation of the packages checked out by the *bash/setup_area.sh* script (which should now be located under *susynt-read/source/*). You can use the ```-h``` or ```--help``` option to see the full list of options:

```
source bash/setup_release.sh --help
```

After running *bash/setup_release.sh* with the ```--compile``` flag you will see the *susynt-read/build/* directory which contains the typical ```CMake```-like build directory structure. In order to allow all executables be in the user's path, the *bash/setup_release.sh* script sources the *setup.sh* script located in *susynt-read/build/x86_64-\*/* directory.

### Subsequent Area Setup

If you are returning to your *susynt-read/* directory from a new shell and you have previously compiled all of the software, you need to still setup the environment so that all of the executables, librarires, etc... can be found. You can do this simply by calling the *bash/setup_release.sh* script with no arguments:

```
source bash/setup_release.sh
```

This sources the *setup.sh* script located in *susynt-read/build/x86_64-\*/* directory.

### Subsequent Compilation

You can either call:

```
source bash/setup_release.sh --compile
```

every time you wish to compile. But this runs the *cmake* command to initiate the ```CMake``` configuration steps. This also removes the previous *build/* directory and starts a new one.

The simpler and faster way (and therefore recommended way) is to move to the *build/* directory and simply call ```make```:

```
cd build/
make
```

### Compiling After Changes to CMakeLists

If you change any of the ```CMakeLists.txt``` files in any of the packages in *susynt-read/source/* directory, you need to re-run the ```CMake``` configuration. You can do this simply by running:

```
source bash/setup_release.sh --compile
```

or, if you do not want to completely remove the previous *build/* directory (and are sure that your changes are OK for this) you can simply do:

```
cd build/
cmake ../source
make
```

## Useful Scripts

### Listing Available Datasets
There is the ```python``` script *python/available_datasets* which will provide you with text files for all of the **mc** and **data** susyNt samples for the given production tag. Run it as follows:

```
./python/available_datasets
```

or

```
python python/available_datasets
```

### Making Condor Filelists
You can use the text files produced by the *python/available_datasets* script to produce condor-based filelists. You can do this with the *python/make_condor_lists.py* script:

```
python python/make_condor_lists.py -i <text-file> -o <filelist-directory>
```

where ```<text-file>``` is one of the text files produced by the *python/available_datasets* script (or a text file with a listing of the susyNt samples, e.g. from ```rucio ls```) and ```<filelist-directory>``` is a directory where the *python/make_condor_lists.py* script will place all of the output. The output of this script are text files which represent filelists where each file in a dataset is the **global logical filename** used by condor.

### Making a Simple Looper
For simple tests, initialize a new looper using the make_susy_skeleton script in SusyNtuple: 

```
./SusyNtuple/scripts/make_susy_skeleton -x mySusyNtLooperExecutable -c MySusyNtLooper
```

Go back to the root directory and rebuild the package (inside susynt-read: `source bash/setup_release.sh --compile`).
Build a test directory (preferably outside of the source directory) and then run your executable inside it:

```
mkdir testLooper
cd testLooper
mySusyNtLooperExecutable -i path/to/SusyNt/file.root
```

The looper should print out some basic event information
