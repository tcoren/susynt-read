#!/bin/bash

##############################################################
# setup_area
#
# script to checkout the packages necessary for setting
# up an area to read susyNt
#
# daniel.joseph.antrim@cern.ch
# August 2017
#
#############################################################

base_release="AnalysisBase,21.2.60,here"
athena_tag="release/21.2.60"
default_gitcache="${HOME}/susynt_gitcache/athena"
stable_sn_tag="SusyNtuple-00-08-03"

function print_usage {
    echo "------------------------------------------------"
    echo " setup_area"
    echo ""
    echo " Options:"
    echo "  --gitache               Cache location to use for athena [default:${default_gitcache}]"
    echo "  --no-cache              Do not use a git cache, do full sparse checkout instead [default: false]"
    echo "  --master                Checkout master branch of SusyNtuple"
    echo "  --sn                    Set branch/tag to checkout for SusyNtuple"
    echo "  -h|--help               Print this help message"
    echo ""
    echo " Example usage:"
    echo "  - Setup the area for stable use (reading n0306 susyNt):"
    echo "   $ source setup_area.sh"
    echo "  - Checkout branch 'dev' of SusyNtuple:"
    echo "   $ source setup_area.sh --sn dev"
    echo "------------------------------------------------"
}

function prepare_area {
    dirname="source/"
    if [[ -d "$dirname" ]]; then
        echo "setup_area    $dirname directory exists"
    else
        echo "setup_area    Creating $dirname directory"
    fi
    mkdir -p $dirname
}

function get_externals {

    cache_location=${1}
    dont_use_cache=${2}

    startdir=${PWD}
    sourcedir="./source/"
    if [[ -d $sourcedir ]]; then
        cd $sourcedir
    else
        echo "setup_area    ERROR no $sourcedir directory"
        return 1
    fi

    sourcedir=${PWD}
    echo "setup_area    Setting up AnalysisBase release: $base_release"
    lsetup "asetup $base_release"

    lsetup git
    if [[ $dont_use_cache == 0 ]]; then
        echo "setup_area    Calling: 'git atlas init-workdir $cache_location'"
        git atlas init-workdir $cache_location
    else
        echo "setup_area    Calling: 'git atlas init-workdir https://:@gitlab.cern.ch:8443/susynt/athena.git'"
        git atlas init-workdir https://:@gitlab.cern.ch:8443/susynt/athena.git
    fi

    patchfile=${startdir}/patchSUSYTools.patch

    if [[ -d "./athena/" ]]; then
        cd ./athena/
        echo "setup_area    Checking out athena tag: $athena_tag"
        git fetch upstream
        git checkout $athena_tag
        git atlas addpkg SUSYTools

        #cp $patchfile .
        #git apply patchSUSYTools.patch

        cd $sourcedir
    else
        echo "setup_area    ERROR Did not get ATLAS sw repository"
        cd $startdir
        return 1
    fi

    rmdir="./athena/Projects/"
    if [[ -d $rmdir ]]; then
        cmd="rm -r $rmdir"
        $cmd
    fi


    cd $startdir
}

function get_susynt {
    tag=${1}
    dirname="./source/"
    startdir=${PWD}
    if [[ -d $dirname ]]; then
        cd $dirname
    else
        echo "setup_area    ERROR $dirname directory not found when checking out SusyNtuple"
        cd $startdir
        return 1
    fi
    git clone -b master https://:@gitlab.cern.ch:8443/susynt/SusyNtuple.git
    cd ./SusyNtuple
    git checkout $tag
    cd $startdir
}

function main {

    sn_tag=$stable_sn_tag
    gitcache=$default_gitcache
    no_cache=0

    while test $# -gt 0
    do
        case $1 in
            --master)
                sn_tag="master"
                ;;
            --gitcache)
                gitcache=${2}
                shift
                ;;
            --no-cache)
                no_cache=1
                ;;
            -h)
                print_usage
                return 0
                ;;
            --help)
                print_usage
                return 0
                ;;
            *)
                echo "ERROR Invalid argument: $1"
                return 1
                ;;
        esac
        shift
    done

    echo "setup_area    Starting -- `date`"
    if [[ $no_cache == 1 ]]; then
        echo "setup_area    Will perform sparse checkout of athena (not using cache)"
    else
        echo "setup_area    Using gitcache                      : $gitcache"
    fi
    echo "setup_area    Checking out SusyNtuple release     :  $sn_tag"

    prepare_area
    if ! get_externals $gitcache $no_cache ; then return 1; fi
    if ! get_susynt $sn_tag ; then return 1; fi
}

#_______________
main $*
